file "/etc/hosts" do
  owner 'root'
  group 'root'
  mode 0755
  content IO.read("/home/ubuntu/hosts")
  action :create
end